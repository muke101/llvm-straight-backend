//===-- STRAIGHT.h - Top-level interface for Cpu0 representation ----*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the entry points for global functions defined in
// the LLVM STRAIGHT back-end.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_STRAIGHT_CPU0_H
#define LLVM_LIB_TARGET_STRAIGHT_CPU0_H

#include "MCTargetDesc/STRAIGHTMCTargetDesc.h"
#include "llvm/Target/TargetMachine.h"

namespace llvm {
  class STRAIGHTTargetMachine;
  class FunctionPass;

} // end namespace llvm;

#endif
