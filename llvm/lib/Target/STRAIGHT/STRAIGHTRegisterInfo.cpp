//===-- STRAIGHTRegisterInfo.cpp - STRAIGHT Register Information ------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the STRAIGHT implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#include "STRAIGHTRegisterInfo.h"
#include "STRAIGHT.h"
#include "STRAIGHTMachineFunctionInfo.h"
#include "STRAIGHTSubtarget.h"
#include "llvm/BinaryFormat/Dwarf.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/RegisterScavenging.h"
#include "llvm/CodeGen/TargetFrameLowering.h"
#include "llvm/CodeGen/TargetInstrInfo.h"
#include "llvm/IR/DebugInfoMetadata.h"
#include "llvm/Support/ErrorHandling.h"

#define GET_REGINFO_TARGET_DESC
#include "STRAIGHTGenRegisterInfo.inc"

using namespace llvm;

STRAIGHTRegisterInfo::STRAIGHTRegisterInfo(unsigned HwMode)
    : STRAIGHTGenRegisterInfo(STRAIGHT::I0, /*DwarfFlavour*/0, /*EHFlavor*/0,
                           /*PC*/0, HwMode) {} //FIXME: no idea whats going on here
