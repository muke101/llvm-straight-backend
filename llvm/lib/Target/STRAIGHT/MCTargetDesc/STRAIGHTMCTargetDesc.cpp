//===-- STRAIGHTMCTargetDesc.cpp - STRAIGHT Target Descriptions -----------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
///
/// This file provides STRAIGHT-specific target descriptions.
///
//===----------------------------------------------------------------------===//

#include "STRAIGHTMCTargetDesc.h"
#include "STRAIGHTBaseInfo.h"
#include "STRAIGHTELFStreamer.h"
#include "STRAIGHTInstPrinter.h"
#include "STRAIGHTMCAsmInfo.h"
#include "STRAIGHTMCObjectFileInfo.h"
#include "STRAIGHTTargetStreamer.h"
#include "TargetInfo/STRAIGHTTargetInfo.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/MC/MCAsmBackend.h"
#include "llvm/MC/MCAsmInfo.h"
#include "llvm/MC/MCCodeEmitter.h"
#include "llvm/MC/MCInstrAnalysis.h"
#include "llvm/MC/MCInstrInfo.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCObjectWriter.h"
#include "llvm/MC/MCRegisterInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/MC/TargetRegistry.h"
#include "llvm/Support/ErrorHandling.h"

#define GET_INSTRINFO_MC_DESC
#include "STRAIGHTGenInstrInfo.inc"

#define GET_REGINFO_MC_DESC
#include "STRAIGHTGenRegisterInfo.inc"

#define GET_SUBTARGETINFO_MC_DESC
#include "STRAIGHTGenSubtargetInfo.inc"

using namespace llvm;

#define DEBUG_TYPE "straight"

extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeSTRAIGHTTargetMC() {
}
