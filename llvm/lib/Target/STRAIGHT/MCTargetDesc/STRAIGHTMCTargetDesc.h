//===-- STRAIGHTMCTargetDesc.h - STRAIGHT Target Descriptions ---------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file provides STRAIGHT specific target descriptions.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_STRAIGHT_MCTARGETDESC_RISCVMCTARGETDESC_H
#define LLVM_LIB_TARGET_STRAIGHT_MCTARGETDESC_RISCVMCTARGETDESC_H

#include "llvm/Config/config.h"
#include "llvm/MC/MCTargetOptions.h"
#include "llvm/Support/DataTypes.h"
#include <memory>

namespace llvm {
class Target;
class Triple;

extern Target TheSTRAIGHTTarget;

}

// Defines symbolic names for STRAIGHT registers.
#define GET_REGINFO_ENUM
#include "STRAIGHTGenRegisterInfo.inc"

// Defines symbolic names for STRAIGHT instructions.
#define GET_INSTRINFO_ENUM
#include "STRAIGHTGenInstrInfo.inc"

#define GET_SUBTARGETINFO_ENUM
#include "STRAIGHTGenSubtargetInfo.inc"

#endif
