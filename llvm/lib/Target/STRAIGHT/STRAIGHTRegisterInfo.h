//===-- STRAIGHTRegisterInfo.h - STRAIGHT Register Information Impl ---*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the STRAIGHT implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_STRAIGHT_STRAIGHTREGISTERINFO_H
#define LLVM_LIB_TARGET_STRAIGHT_STRAIGHTREGISTERINFO_H

#include "llvm/CodeGen/TargetRegisterInfo.h"

#define GET_REGINFO_HEADER
#include "STRAIGHTGenRegisterInfo.inc"

namespace llvm {

struct STRAIGHTRegisterInfo : public STRAIGHTGenRegisterInfo {

  STRAIGHTRegisterInfo(unsigned HwMode);

};
}

#endif
