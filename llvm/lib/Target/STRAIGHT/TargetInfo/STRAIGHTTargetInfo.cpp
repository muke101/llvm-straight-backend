//===-- STRAIGHTTargetInfo.cpp - STRAIGHT Target Implementation -----------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "STRAIGHTTargetInfo.h"
#include "STRAIGHT.h"
#include "llvm/IR/Module.h"
#include "llvm/MC/TargetRegistry.h"
using namespace llvm;

extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeSparcTargetInfo() {
  RegisterTarget<Triple::straight, /*HasJIT=*/false> X(getTheSTRAIGHTTarget(), "straight",
                                                        "STRAIGHT", "STRAIGHT");
}

Target &llvm::getTheSTRAIGHTTarget() {
  static Target TheSTRAIGHTTarget;
  return TheSTRAIGHTTarget;
}
