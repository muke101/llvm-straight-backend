//===-- STRAIGHTInstrInfo.h - STRAIGHT Instruction Information --------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the STRAIGHT implementation of the TargetInstrInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_STRAIGHT_STRAIGHTINSTRINFO_H
#define LLVM_LIB_TARGET_STRAIGHT_STRAIGHTINSTRINFO_H

#include "STRAIGHTRegisterInfo.h"
#include "llvm/CodeGen/TargetInstrInfo.h"
#include "llvm/IR/DiagnosticInfo.h"

#define GET_INSTRINFO_HEADER
#define GET_INSTRINFO_OPERAND_ENUM
#include "STRAIGHTGenInstrInfo.inc"

namespace llvm {

class STRAIGHTSubtarget;

namespace STRAIGHTCC {

enum CondCode {
  COND_EQ,
  COND_NE,
  COND_LT,
  COND_GE,
  COND_LTU,
  COND_GEU,
  COND_INVALID
};

CondCode getOppositeBranchCondition(CondCode);

} // end of namespace STRAIGHTCC

class STRAIGHTInstrInfo : public STRAIGHTGenInstrInfo {

public:
  explicit STRAIGHTInstrInfo(STRAIGHTSubtarget &STI);

  MCInst getNop() const override;

  unsigned isLoadFromStackSlot(const MachineInstr &MI,
                               int &FrameIndex) const override;
  unsigned isStoreToStackSlot(const MachineInstr &MI,
                              int &FrameIndex) const override;

  void storeRegToStackSlot(MachineBasicBlock &MBB,
                           MachineBasicBlock::iterator MBBI, Register SrcReg,
                           bool IsKill, int FrameIndex,
                           const TargetRegisterClass *RC,
                           const TargetRegisterInfo *TRI) const override;

  void loadRegFromStackSlot(MachineBasicBlock &MBB,
                            MachineBasicBlock::iterator MBBI, Register DstReg,
                            int FrameIndex, const TargetRegisterClass *RC,
                            const TargetRegisterInfo *TRI) const override;
};

namespace STRAIGHT {

// Implemented in STRAIGHTGenInstrInfo.inc
int16_t getNamedOperandIdx(uint16_t Opcode, uint16_t NamedIndex);

// Special immediate for AVL operand of V pseudo instructions to indicate VLMax.
static constexpr int64_t VLMaxSentinel = -1LL;
} // namespace STRAIGHT

namespace STRAIGHTVPseudosTable {

struct PseudoInfo {
  uint16_t Pseudo;
  uint16_t BaseInstr;
};

#define GET_STRAIGHTVPseudosTable_DECL
#include "STRAIGHTGenSearchableTables.inc"

} // end namespace STRAIGHTVPseudosTable

} // end namespace llvm
#endif
