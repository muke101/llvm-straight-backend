//===-- STRAIGHTInstrInfo.cpp - STRAIGHT Instruction Information ------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the STRAIGHT implementation of the TargetInstrInfo class.
//
//===----------------------------------------------------------------------===//

#include "STRAIGHTInstrInfo.h"
#include "MCTargetDesc/STRAIGHTMatInt.h"
#include "STRAIGHT.h"
#include "STRAIGHTMachineFunctionInfo.h"
#include "STRAIGHTSubtarget.h"
#include "STRAIGHTTargetMachine.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/MemoryLocation.h"
#include "llvm/CodeGen/LiveIntervals.h"
#include "llvm/CodeGen/LiveVariables.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/RegisterScavenging.h"
#include "llvm/MC/MCInstBuilder.h"
#include "llvm/MC/TargetRegistry.h"
#include "llvm/Support/ErrorHandling.h"

using namespace llvm;

#define GEN_CHECK_COMPRESS_INSTR
#include "STRAIGHTGenCompressInstEmitter.inc"

#define GET_INSTRINFO_CTOR_DTOR
#define GET_INSTRINFO_NAMED_OPS
#include "STRAIGHTGenInstrInfo.inc"

static cl::opt<bool> PreferWholeRegisterMove(
    "straight-prefer-whole-register-move", cl::init(false), cl::Hidden,
    cl::desc("Prefer whole register move for vector registers."));

namespace llvm {
namespace STRAIGHTVPseudosTable {

using namespace STRAIGHT;

#define GET_STRAIGHTVPseudosTable_IMPL
#include "STRAIGHTGenSearchableTables.inc"

} // namespace STRAIGHTVPseudosTable
} // namespace llvm

STRAIGHTInstrInfo::STRAIGHTInstrInfo(STRAIGHTSubtarget &STI)
    : STRAIGHTGenInstrInfo(STRAIGHT::ADJCALLSTACKDOWN, STRAIGHT::ADJCALLSTACKUP),
      STI(STI) {}

MCInst STRAIGHTInstrInfo::getNop() const {
  if (STI.getFeatureBits()[STRAIGHT::FeatureStdExtC])
    return MCInstBuilder(STRAIGHT::C_NOP);
  return MCInstBuilder(STRAIGHT::ADDI)
      .addReg(STRAIGHT::X0)
      .addReg(STRAIGHT::X0)
      .addImm(0);
}

unsigned STRAIGHTInstrInfo::isLoadFromStackSlot(const MachineInstr &MI,
                                             int &FrameIndex) const {
  switch (MI.getOpcode()) {
  default:
    return 0;
  case STRAIGHT::LB:
  case STRAIGHT::LBU:
  case STRAIGHT::LH:
  case STRAIGHT::LHU:
  case STRAIGHT::FLH:
  case STRAIGHT::LW:
  case STRAIGHT::FLW:
  case STRAIGHT::LWU:
  case STRAIGHT::LD:
  case STRAIGHT::FLD:
    break;
  }

  if (MI.getOperand(1).isFI() && MI.getOperand(2).isImm() &&
      MI.getOperand(2).getImm() == 0) {
    FrameIndex = MI.getOperand(1).getIndex();
    return MI.getOperand(0).getReg();
  }

  return 0;
}

unsigned STRAIGHTInstrInfo::isStoreToStackSlot(const MachineInstr &MI,
                                            int &FrameIndex) const {
  switch (MI.getOpcode()) {
  default:
    return 0;
  case STRAIGHT::SB:
  case STRAIGHT::SH:
  case STRAIGHT::SW:
  case STRAIGHT::FSH:
  case STRAIGHT::FSW:
  case STRAIGHT::SD:
  case STRAIGHT::FSD:
    break;
  }

  if (MI.getOperand(1).isFI() && MI.getOperand(2).isImm() &&
      MI.getOperand(2).getImm() == 0) {
    FrameIndex = MI.getOperand(1).getIndex();
    return MI.getOperand(0).getReg();
  }

  return 0;
}

void STRAIGHTInstrInfo::storeRegToStackSlot(MachineBasicBlock &MBB,
                                         MachineBasicBlock::iterator I,
                                         Register SrcReg, bool IsKill, int FI,
                                         const TargetRegisterClass *RC,
                                         const TargetRegisterInfo *TRI) const {
  DebugLoc DL;
  if (I != MBB.end())
    DL = I->getDebugLoc();

  MachineFunction *MF = MBB.getParent();
  MachineFrameInfo &MFI = MF->getFrameInfo();

  unsigned Opcode;
  bool IsScalableVector = true;
  bool IsZvlsseg = true;
  if (STRAIGHT::GPRRegClass.hasSubClassEq(RC)) {
    Opcode = TRI->getRegSizeInBits(STRAIGHT::GPRRegClass) == 32 ?
             STRAIGHT::SW : STRAIGHT::SD;
    IsScalableVector = false;
  } else if (STRAIGHT::FPR16RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::FSH;
    IsScalableVector = false;
  } else if (STRAIGHT::FPR32RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::FSW;
    IsScalableVector = false;
  } else if (STRAIGHT::FPR64RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::FSD;
    IsScalableVector = false;
  } else if (STRAIGHT::VRRegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVSPILL_M1;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRM2RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVSPILL_M2;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRM4RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVSPILL_M4;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRM8RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVSPILL_M8;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRN2M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL2_M1;
  else if (STRAIGHT::VRN2M2RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL2_M2;
  else if (STRAIGHT::VRN2M4RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL2_M4;
  else if (STRAIGHT::VRN3M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL3_M1;
  else if (STRAIGHT::VRN3M2RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL3_M2;
  else if (STRAIGHT::VRN4M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL4_M1;
  else if (STRAIGHT::VRN4M2RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL4_M2;
  else if (STRAIGHT::VRN5M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL5_M1;
  else if (STRAIGHT::VRN6M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL6_M1;
  else if (STRAIGHT::VRN7M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL7_M1;
  else if (STRAIGHT::VRN8M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVSPILL8_M1;
  else
    llvm_unreachable("Can't store this register to stack slot");

  if (IsScalableVector) {
    MachineMemOperand *MMO = MF->getMachineMemOperand(
        MachinePointerInfo::getFixedStack(*MF, FI), MachineMemOperand::MOStore,
        MemoryLocation::UnknownSize, MFI.getObjectAlign(FI));

    MFI.setStackID(FI, TargetStackID::ScalableVector);
    auto MIB = BuildMI(MBB, I, DL, get(Opcode))
                   .addReg(SrcReg, getKillRegState(IsKill))
                   .addFrameIndex(FI)
                   .addMemOperand(MMO);
    if (IsZvlsseg) {
      // For spilling/reloading Zvlsseg registers, append the dummy field for
      // the scaled vector length. The argument will be used when expanding
      // these pseudo instructions.
      MIB.addReg(STRAIGHT::X0);
    }
  } else {
    MachineMemOperand *MMO = MF->getMachineMemOperand(
        MachinePointerInfo::getFixedStack(*MF, FI), MachineMemOperand::MOStore,
        MFI.getObjectSize(FI), MFI.getObjectAlign(FI));

    BuildMI(MBB, I, DL, get(Opcode))
        .addReg(SrcReg, getKillRegState(IsKill))
        .addFrameIndex(FI)
        .addImm(0)
        .addMemOperand(MMO);
  }
}

void STRAIGHTInstrInfo::loadRegFromStackSlot(MachineBasicBlock &MBB,
                                          MachineBasicBlock::iterator I,
                                          Register DstReg, int FI,
                                          const TargetRegisterClass *RC,
                                          const TargetRegisterInfo *TRI) const {
  DebugLoc DL;
  if (I != MBB.end())
    DL = I->getDebugLoc();

  MachineFunction *MF = MBB.getParent();
  MachineFrameInfo &MFI = MF->getFrameInfo();

  unsigned Opcode;
  bool IsScalableVector = true;
  bool IsZvlsseg = true;
  if (STRAIGHT::GPRRegClass.hasSubClassEq(RC)) {
    Opcode = TRI->getRegSizeInBits(STRAIGHT::GPRRegClass) == 32 ?
             STRAIGHT::LW : STRAIGHT::LD;
    IsScalableVector = false;
  } else if (STRAIGHT::FPR16RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::FLH;
    IsScalableVector = false;
  } else if (STRAIGHT::FPR32RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::FLW;
    IsScalableVector = false;
  } else if (STRAIGHT::FPR64RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::FLD;
    IsScalableVector = false;
  } else if (STRAIGHT::VRRegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVRELOAD_M1;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRM2RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVRELOAD_M2;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRM4RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVRELOAD_M4;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRM8RegClass.hasSubClassEq(RC)) {
    Opcode = STRAIGHT::PseudoVRELOAD_M8;
    IsZvlsseg = false;
  } else if (STRAIGHT::VRN2M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD2_M1;
  else if (STRAIGHT::VRN2M2RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD2_M2;
  else if (STRAIGHT::VRN2M4RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD2_M4;
  else if (STRAIGHT::VRN3M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD3_M1;
  else if (STRAIGHT::VRN3M2RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD3_M2;
  else if (STRAIGHT::VRN4M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD4_M1;
  else if (STRAIGHT::VRN4M2RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD4_M2;
  else if (STRAIGHT::VRN5M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD5_M1;
  else if (STRAIGHT::VRN6M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD6_M1;
  else if (STRAIGHT::VRN7M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD7_M1;
  else if (STRAIGHT::VRN8M1RegClass.hasSubClassEq(RC))
    Opcode = STRAIGHT::PseudoVRELOAD8_M1;
  else
    llvm_unreachable("Can't load this register from stack slot");

  if (IsScalableVector) {
    MachineMemOperand *MMO = MF->getMachineMemOperand(
        MachinePointerInfo::getFixedStack(*MF, FI), MachineMemOperand::MOLoad,
        MemoryLocation::UnknownSize, MFI.getObjectAlign(FI));

    MFI.setStackID(FI, TargetStackID::ScalableVector);
    auto MIB = BuildMI(MBB, I, DL, get(Opcode), DstReg)
                   .addFrameIndex(FI)
                   .addMemOperand(MMO);
    if (IsZvlsseg) {
      // For spilling/reloading Zvlsseg registers, append the dummy field for
      // the scaled vector length. The argument will be used when expanding
      // these pseudo instructions.
      MIB.addReg(STRAIGHT::X0);
    }
  } else {
    MachineMemOperand *MMO = MF->getMachineMemOperand(
        MachinePointerInfo::getFixedStack(*MF, FI), MachineMemOperand::MOLoad,
        MFI.getObjectSize(FI), MFI.getObjectAlign(FI));

    BuildMI(MBB, I, DL, get(Opcode), DstReg)
        .addFrameIndex(FI)
        .addImm(0)
        .addMemOperand(MMO);
  }
}
